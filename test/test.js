"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const threader_1 = require("../threader");
let longTask = (n, a) => {
    return () => new Promise((resolve, reject) => {
        setTimeout(() => {
            return resolve(a);
        }, n * 1000);
    });
};
let n = 10;
let array = Array.apply(null, { length: n }).map(Number.call, Number);
let testVals = array.map(a => longTask(2, a));
new threader_1.Threader(3).start(testVals)
    .then(res => {
    console.log("Finished: " + res);
})
    .catch(err => console.error(err));
//# sourceMappingURL=test.js.map