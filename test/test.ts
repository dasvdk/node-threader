import { Threader, Action } from "../threader"

let longTask = (n: number, a: number) => {
    return () =>  new Promise<number>((resolve, reject) => {
        setTimeout(() => {
            return resolve(a)
        }, n * 1000 )
    })
}

let n = 10
let array : number[] = Array.apply(null, { length: n }).map(Number.call, Number)
let testVals : Action<number>[] = array.map(a => longTask(2, a))
new Threader(3).start(testVals)
   .then(res => {
       console.log("Finished: " + res)
   })
   .catch(err => console.error(err))

