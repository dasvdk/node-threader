export type Action<T> = () => Promise<T>
export type NextAction<T> = () => Action<T> | null
export class Threader<T>{
    threadCount: number
    constructor(threadCount : number) {
        this.threadCount = threadCount || 5
    }
    
    startChain(next : NextAction<T>) : Promise<T[]> {
        let task = next()
        if (task) {
            //console.log("Starting task")
            let a = task()
            let b = a.then(res => this.startChain(next))
            return Promise.all([a,b])
                .then(function([resA, resB]) {
                    return [ resA ].concat(resB)
                })
        } else {
            return Promise.resolve([])
        }
    }

    start(actions : Action<T>[]) : Promise<T[]> {
        let next: NextAction<T> = () => actions.length ? actions.shift() : null
        let _results : Promise<T[]>[] = []
        for (let i = 0; i < this.threadCount; i++)
        {
            //console.log("Starting thread: " + i)
            _results.push(this.startChain(next));
        }
        return Promise.all(_results)
            .then(values => [].concat.apply([], values))
    }
}

