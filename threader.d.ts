export declare type Action<T> = () => Promise<T>;
export declare type NextAction<T> = () => Action<T> | null;
export declare class Threader<T> {
    threadCount: number;
    constructor(threadCount: number);
    startChain(next: NextAction<T>): Promise<T[]>;
    start(actions: Action<T>[]): Promise<T[]>;
}
