"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Threader {
    constructor(threadCount) {
        this.threadCount = threadCount || 5;
    }
    startChain(next) {
        let task = next();
        if (task) {
            //console.log("Starting task")
            let a = task();
            let b = a.then(res => this.startChain(next));
            return Promise.all([a, b])
                .then(function ([resA, resB]) {
                return [resA].concat(resB);
            });
        }
        else {
            return Promise.resolve([]);
        }
    }
    start(actions) {
        let next = () => actions.length ? actions.shift() : null;
        let _results = [];
        for (let i = 0; i < this.threadCount; i++) {
            //console.log("Starting thread: " + i)
            _results.push(this.startChain(next));
        }
        return Promise.all(_results)
            .then(values => [].concat.apply([], values));
    }
}
exports.Threader = Threader;
//# sourceMappingURL=threader.js.map